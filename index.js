require('dotenv').config()
var express = require('express');
var axios = require('axios');
var bodyParser = require('body-parser');
const  { pokeApi } = require('./endPoints');
var app = express();
var cors = require('cors');

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
const userHandlers = pokeApi({axios})

app.get('/getPokemons', userHandlers.getPokemons);
app.get('/getUsers', userHandlers.getUsersDB);
app.post('/createUser', userHandlers.createUserDB);
app.put('/updatePokeLoved', userHandlers.updatePokeLovedDB);
app.put('/removePokeLoved', userHandlers.removePokeLovedDB);
app.get('/user/:id', userHandlers.getUserDB);

app.listen(3001, function () {
  console.log('Listening on port 3001!');
});
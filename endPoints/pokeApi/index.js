const { URL_POKE_API } = process.env
const connectDb = require('../db')
const { ObjectID } = require('mongodb')



const handlers = ({ axios }) => ({
    getPokemons: async (req, res) => {
        const { data } = await axios.get(URL_POKE_API)
          res.send(data);
    },
    getUsersDB: async (req, res) => {
        let db
        let users = []
        try {
          db = await connectDb()
          users = await db.collection('pokeUsers').find().toArray()
        } catch (error) {
          console.error(error)
        }
        res.send(users);
      },
    createUserDB: async (req, res) => {
      const { body } = req;

      const defaults = {
        name:"dafaultName",
        pokeLoved: []       
      }
  
      const newUser = {...defaults, ...body}
      let db
      let course
      
      try {
        db = await connectDb()
          await db.collection('pokeUsers').findOne({
            name:body.name
        })
        .then(async (a)=>{if(a === null) { 
          course = await db.collection('pokeUsers').insertOne(newUser)
          newUser._id = course.insertedId
        } else {
          newUser._id = a._id
      }})
        
      } catch (error) {
        console.error(error)
      }
      res.status(200).send(newUser);
  
    },
    updatePokeLovedDB: async (req, res) => {
        const { id : _id, pokeLoved } = req.body; // id , pokemon
        let db
        try {
          db = await connectDb()
          await db.collection('pokeUsers').updateOne(
            { _id: ObjectID(_id) },
              {
                $push:{ 
                  pokeLoved
                }
              }
          )
          
        } catch (error) {
          console.error(error)
        }
        res.sendStatus(204)
    },
    removePokeLovedDB: async (req, res) => {
        const { id : _id, pokeLoved } = req.body; // id , pokemon
        let db
        try {
          db = await connectDb()
          await db.collection('pokeUsers').updateOne(
            { _id: ObjectID(_id) },
              {
                $pull:{ 
                  pokeLoved: { name: pokeLoved.name }
                }
              }
          )
          
        } catch (error) {
          console.error(error)
        }
        res.sendStatus(204)
    },

    getUserDB: async (req, res) => {
        const { id } = req.params;
        
        let db
        let user
        
        try {
          db = await connectDb()
          user = await db.collection('pokeUsers').findOne({
            _id:  ObjectID(id)
          })
        } catch (error) {
          console.error(error)
        }
        res.send(user);
    },
})

module.exports =  handlers

